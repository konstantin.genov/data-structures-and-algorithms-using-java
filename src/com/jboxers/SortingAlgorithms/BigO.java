package com.jboxers.SortingAlgorithms;


public class BigO {


    /* We look at the number of steps it takes to execute an algorithm. We call this the time complexity.
        There are two types of complexity:
        - Time complexity: the number of steps involved to run an algorithm
        - Memory complexity: the amount of memory it takes to run an algorithm. Memory complexity is not
        a big issue this days as memory is now cheap and abundant.

        These days we mainly concern ourselves with time complexity. So we are interested in how many steps does
        it take to run an algorithm. When we are doing this, we should look at the worst case - looking at the
        best case doesn't help us because you are rarely going to have the best case. Now, we could take the
        average case but that's not gonna tell us the absolute worse time complexities, we want to know what
        the upper bond is(what is the absolute worst)
     */

    /*
    For example adding sugar to tea:
    1. Fetch the bowl containing the sugar
    2. Get a spoon
    3. SCoop out sugar using the spoon
    4. Pour the sugar from the spoon into the tea.
    5. Repeat steps 3 and 4 until you've added the desired amount of sugar

    The number of steps it is going to take to add sugar to your tea is going to depend on how many
    sugars you want. If you want 1 sugar, it will take you 4 steps, 2 sugars 6 steps, etc.

    The Big O notation is a way of expressing the complexity related to the number of items that an algorithm has to deal with.
    Working out the Big O value for our adding sugar algorithm:
    - Number of sugars is n
    - Steps = 2*n + 2
    - 3 sugars = (2*3) + 2 => 8
    Time complexity is O(n) - linear
     */
}
