package com.jboxers.SortingAlgorithms;

import java.util.ArrayList;
import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {

        int[] intArr = {20, 30, -15, 7, 55, 1, -22};
        new ArrayList<Integer>().add(1);

        // We shift (bubble) an element to left or right depending by what order we are sorting
        // The worst case performance of the algorithm and average complexity is О(n^2)
        // It will take 100 steps to sort 10 items, 10,000 steps to sort 100, etc..
        for (int lastUnsortedIndex = intArr.length - 1; lastUnsortedIndex > 0;
             lastUnsortedIndex--) {
            for (int i = 0; i < lastUnsortedIndex; i++) {
                if (intArr[i] > intArr[i + 1]) {
//                    int temp = intArr[i];
//                    intArr[i] = intArr[i + 1];
//                    intArr[i + 1] = temp;
                    swap(intArr, i, i + 1);
                }
            }
        }
        System.out.println(Arrays.toString(intArr));

    }

    public static void swap(int[] arr, int i, int j) {
        if (i == j) {
            return;
        }

        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

}
