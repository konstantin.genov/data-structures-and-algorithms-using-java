package com.jboxers.SortingAlgorithms;

import java.util.ArrayList;
import java.util.Arrays;

public class MergeSort {

    /*
    Merge sort is a divine and conquer algorithm because it involves splitting
    the array you want to sort in a bunch of smaller arrays, it is usually implemented
    by using recursion. The algorithm involves 2 phases:
    Phase 1 - splitting phase: logical, we don't create any new arrays
    Phase 2 - merging phase (sorting)

    Spitting phase:
    We start with an unsorted array and we divide it into two arrays, which are unsorted. The first array is the left array,
    and the second array is the right array. We split the left and right arrays into two arrays each
    and we keep splitting until all the arrays have only one element each - these arrays are sorted
     */


    public static void main(String[] args) {
        int[] intArray = {20, 35, -15, 7, 55, 1, -22};
        mergeSort(intArray, 0, intArray.length);

    }

    // 20, 35, -15, 7, 55, 1, -22
    static void mergeSort(int arrayToSort[], int startIndex, int endIndex) {
        if (endIndex - startIndex < 2) {
            System.out.println("End of recursion");
            return; // break recursion when we reach a 1 element array
        }

        int middleIndex = (startIndex + endIndex) / 2;
        System.out.println("Array before mergeSort recursion: " + Arrays.toString(arrayToSort));
        System.out.println("Middle point index: " + middleIndex);
        mergeSort(arrayToSort, startIndex, middleIndex); // logically sort left side first
        mergeSort(arrayToSort, middleIndex, endIndex); // right side

        merge(arrayToSort, startIndex, middleIndex, endIndex);
    }

    // 20, 35, -15, 7, 55, 1, -22
    static void merge(int[] input, int startIndex, int middleIndex, int endIndex) {
        if (input[middleIndex - 1] <= input[middleIndex]) return; // if arrays are already sorted, break


        int i = startIndex;
        int j = middleIndex;
        int tempIndex = 0;

        int[] tempArray = new int[endIndex - startIndex];

        while (i < middleIndex && j < endIndex) {
            tempArray[tempIndex++] = input[i] <= input[j] ? input[i++] : input[j++];
        }
        System.out.println("Calling Merge");

    }
}
