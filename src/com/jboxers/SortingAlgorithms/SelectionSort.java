package com.jboxers.SortingAlgorithms;

import java.util.Arrays;

public class SelectionSort {
    /* This algorithm divides the array into sorted and unsorted partitions
    We traverse the array and look for the largest element in the unsorted partition and we swap it with the last
     element in the unsorted partition. At that point that swapped element will be in its correct position.
     */

    /*
    The selection sort is an in-place algorithm as it doesn't use any extra memory.
    It's a quadratic algorithm - O(n^2) time complexity because we have n elements in the array
    and for each element we traverse n elements, however it doesn't require much swapping like bubble sort.
    In the average case the selection sort will perform better than bubble sort.
    SelectionSort is an unstable algorithm as there is no guarantee that the original positions of equal elements
    will be preserved. [DO NOT USE WITH OBJECTS]
     */

    public static void main(String[] args) {
        int[] intArray = {20, 35, -15, 7, 55, 1, -22};

        // We traverse n elements by n elements, so 10 elements by 10 times = 100 times
        for (int lastUnsortedIndex = intArray.length - 1; lastUnsortedIndex > 0; lastUnsortedIndex--) {

            int largestIndex = 0; // We always start and index 0 as we assume that the first element is the largest

            for (int i = 0; i <= lastUnsortedIndex; i++) {
                if (intArray[i] > intArray[largestIndex]) { // if/when we find a bigger element
                    largestIndex = i; // change index location
                }
            }
            swap(intArray, largestIndex, lastUnsortedIndex); //swap after traversing all elements

        }

        System.out.println((Arrays.toString(intArray)));
    }

    private static void swap(int[] array, int i, int j) {
        if (i == j) {
            return;
        }
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

}
