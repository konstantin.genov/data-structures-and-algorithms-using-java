package com.jboxers.SortingAlgorithms;

public class ArraysInMemory {

    /*
    Arrays are are stored as one contiguous block in memory -> you don't have the items/elements of the array scattered
    around memory, they are all stored as one contiguous block. This is why we have to specify the length
     of an array when creating it, that tells the JVM how many bytes of memory to allocate for that array.
     This is one reason why arrays can't be resized - there's no guarantee that the extra space added
     will be in that continuous block in memory.
     */

    /* How array elements are easily accessible:

    For this example, we will use an integer array. An integer is 4 bytes of memory and we know the
    memory address of the start of the array.

    Let's say address of array = 100;

                        4 = 4 bytes of int memory allocation
    array[0] = 100 + (0 * 4) = 100
    array[1] = 100 + (1 * 4) = 104
    array[3523] = 100 + (3523 * 4) = ...
     */
}
