package com.jboxers.SortingAlgorithms;

import java.util.Arrays;

public class InsertionSort {
    /*
    Insertion sort partitions the array into sorted and unsorted partitions, this implementation will grow the
    sorted partition from left to right.
    Q: How does insertion sort work?
    A: It starts out by saying that the element at position 0 is in the sorted partition and because
    the sorted partition is of length 1, by default the element is sorted. At the beginning the elements from
    position 1 to the right are in the unsorted partition. On each iteration we take the first element
    in the unsorted partition, which will be the element of firstUnsortedIndex and we insert it into
    the sorted partition, and at end of each iteration we will have grown the sorted partition by 1 element.
    So after the first iteration the sorted partition's length is 2.

    In-place algorithm, quadratic time complexity O(n^2)
    100 steps to sort 10 items.. etc (n * n)
     */

    public static void main(String[] args) {
        int[] intArray = {20, 35, -15, 7, 55, 1, -22};

        int unsortedIndex, key, sortedIndex;

        // insertion sort always starts at the 1st index (2nd element of array)
        for (unsortedIndex = 1; unsortedIndex < intArray.length; unsortedIndex++) {
            key = intArray[unsortedIndex];  // reference the element at the unsorted index so we can traverse the unsorted elements of the array
            sortedIndex = unsortedIndex - 1; // all elements left of the unsorted index are sorted elements

            // we look for elements bigger than
            while (sortedIndex >= 0 && intArray[sortedIndex] > key) {
                intArray[sortedIndex + 1] = intArray[sortedIndex];
                sortedIndex -= 1;
            }
            // shift biggest element to the right if needed
            intArray[sortedIndex + 1] = key;
        }


        System.out.println(Arrays.toString(intArray));
    }

}
