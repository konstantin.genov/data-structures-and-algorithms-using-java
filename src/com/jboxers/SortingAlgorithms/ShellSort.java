package com.jboxers.SortingAlgorithms;

import java.util.Arrays;

public class ShellSort {
    /*
    In-place algorithm.
    Difficult to nail down the time complexity because it depends on the gap. Worst case O(n^2),
    but can perform better than that.

    Doesn't usually require much shifting as insertion sort, so it usually performs better.

    Unstable algorithm

    The idea behind the shell sort is re-order the array with a some iterations of the gap before
    it reaches 1, when it reaches 1 we perform an insertion sort and the array being somewhat
    sorted, the insertion sort will be much quicker in operation
     */

    public static void main(String[] args) {
        int[] intArray = {20, 35, -15, 7, 55, 1, -22};

        // when gap reaches 1 we are doing insertion sort
        // each iteration before that will help speed up the insertion sort
        for (int gap = intArray.length; gap > 0; gap /= 2) {

            for (int i = gap; i < intArray.length; i++) {
                int key = intArray[i]; // reff element for swapping/inserting
                int j = i; // use for traversing

                while (j >= gap && intArray[j - gap] > key) {
                    intArray[j] = intArray[j - gap]; // replace with bigger element
                    j -= gap;
                }
                // insertion point for the key
                intArray[j] = key;
            }
        }

        System.out.println(Arrays.toString(intArray));
    }
}
