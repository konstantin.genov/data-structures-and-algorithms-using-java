package com.jboxers.DataStructures;

public class Stack {
    /*
    Abstract data type.
    LIFO — Last in, first out.
    Stacks are usually backed by a Linked List.

    Time complexity:
    — O(1) for push, pop, and peek, when using a LL.
    — O(n) for push if using an array, as the array might need to be resized.
     */
}
