package com.jboxers.DataStructures;

public class HashTable {
    /*
    Abstract data type.
    Hash tables provide access to data using keys. (key/value pairs). When you add an item you provide the key and the value.
    When you want to retrieve the item, you provide the key, and from there the hash table can retrieve the value and can do it
    really, really quickly. So, hash tables are optimized for retrieval when you know the key.

    Hash tables are also known as dictionaries, maps, look up tables, associative arrays;

    Hashing:
    — Map keys can be of any data type, but under the cover those keys are being converted to integers. One common way
    of backing a hash table is to use an array. To convert the keys into integers you hash the key.
    A hash function maps the keys to integers, in Java, the hash function is Object.hashCode();

    ** It is possible for the hashing method to produce the same integer value for more than one value — this is known
    as a collision.


    Load factor:
    The load factor tells us how full a hash table is. It is used to decide when to resize the array backing the hashtable.
    We don't want a load factor, which is too low as we have lots of empty space, nor do we want a load factor which is too
    high as it will increase the likelihood of collisions.
     */

}
