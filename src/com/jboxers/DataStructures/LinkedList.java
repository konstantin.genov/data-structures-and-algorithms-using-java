package com.jboxers.DataStructures;

public class LinkedList {
    /*
     The LinkedList is a data structure, containing a list of sequential data objects.
     This implementation is NOT backed by an array.
     In a LL, each item in the list is aware of another item in the list, because each item contains
     a link to the next item in the list. This is different from arrays and lists that are backed by arrays.
      With an array, each item in the list is completely unaware of other items in the array, but items in a LL know,
      which item comes after them, and that means that we have to store extra information in the items(value, reference).
     */
    static void singlyLinkedList() {
        /*
            Singly LL items do not keep a *previous* reference in items, only a *next* pointer.
            All elements are nodes, which have a head.
            Removing/adding elements in the head of the LL will always be a constant time complexity O(1).
            Manipulating data in the middle or the end will be an operation with linear time complexity O(n)
         */
    }

    static void doublyLinkedList() {
        /*
        Doubly LL items have a next and previous fields, which are references to the next and previous elements in the list.
        All elements are node, which have a head and a tail.
        Inserting at head or tail will be a O(1) time complexity operation. However, performing operations in the middle
        of the list (same as SLL) can lead to a linear time complexity, because you will have to traverse from the head or tail
        to find the item in question.
         */
    }
}
