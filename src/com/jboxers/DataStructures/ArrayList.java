package com.jboxers.DataStructures;

public class ArrayList {
    /*
    The ArrayList is a resizable-array implementation of the List interface, meaning that the data in the list is being stored in an array.
    This array is called the *backing* array.

    Benefits: If we know a position of an item in the list, accessing it will be
    efficient. O(1) constant time. ArrayLists are great if all you're going to do is go over them.

    Disadvantages:
    If you want to add a lot of items to an existing AL, this will be slow as the backing array will need to
    be resized by the implementation. Removing items is a slow process as well, as the implementation will have to
    remove the items and shift the remaining items to remove any empty spaces. So, if we are going to be adding a lot of items in an AL, we should have an idea of how many items are going to
    ultimately end up in the list, so you can create an AL instance with a capacity that will accommodate all the items. Otherwise,
     the backing array will have to be re-sized every time it gets full, and you want to add more items to the list.

    Tips:
    You can ensure that the array will be big enough by specifying the capacity. You can use one of the
    default constructors — ArrayList(int initialCapacity) to specify the number of items contained in the
    backing array. If you use an empty constructor, the initialCapacity will be set to 10.

     */
}
