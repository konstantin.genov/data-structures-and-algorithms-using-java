package com.jboxers.DataStructures;

public class Vector {
    /*
    The Vector class has been a part of the JDK since Java 1.0.
    Vector is thread safe, meaning you can use it from multiple threads(to write, delete data)
     without you having to synchronize the code, unlike
    ArrayList, which is not synchronized and manipulating data from multiple threads can lead to a conflict.
     */
}
