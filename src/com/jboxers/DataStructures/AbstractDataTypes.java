package com.jboxers.DataStructures;

public class AbstractDataTypes {
    /*
    List is an abstract data type. It isn't a concrete data structure in the sense that an array is.
    It doesn't dictate how the data is organized. With arrays we have to store the items as one
    continuous block in memory, so arrays are telling us how data is supposed to be stored. Lists don't do that,
    they are more of a conceptual idea. They dictate the operations we can perform on the data set not the items themselves.
    It dictates how we can access the items: can we do random access, can we get to the first item that we added, can
    we get to the last item. An abstract data type is more about behaviour and operations that you can do.

    Any data structure can be used to implement an abstract data type as long as you have a class which implements
    the interface for the abstract data.
     */
}
